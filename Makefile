all:
	sphinx-build -b html -j auto source build

clean:
	rm -rf build

run: all
	xdg-open build/index.html

.PHONY: all clean run
