Hackfest Template
=================

The following text can be used as a starting point when creating hackfest pages (see :doc:`/events/hackfests/organizing`).

:: 

    <Hackfest Title> <Year>
    ===

    * Location: city, country 
    * Dates: 
    * Primary contact: name & email
    * Secondary contact: name & email
    
    ## Description
    
    What will happen at the hackfest. Who it is intended for. What the format will be.
    
    ## Agenda
    
    * Topic 1
    * Topic 2
    * Topic 3
    
    ## Traveling
    
    How to get to the city the event takes place.

    [Travel sponsorship](https://handbook.gnome.org/events/travel.html) is available for those who need it.

    ### By plane:

    * List nearby airports and common flight connections.
    
    ### By train:

    * List local train station or bus routes.

    ### By boat (if applicable):

    * List nearby ports.

    ## Venue
    
    * Address of the venue
    * Rooms
    
    Directions (if necessary).
    
    ## Attendees
    
    | Name            | Role and interests      | Dietary restrictions | Arrives | Departs |
    |-----------------|-------------------------|----------------------|---------|---------|
    |                 |                         |                      |         |         |
    
    ## Blog posts
    
    Please list blog posts about the hackfest below:
    
    * ...
    * ...
    * ...
    
    ## Code of Conduct
    
    Hackfest attendees are required to follow the [GNOME Code of Conduct](https://conduct.gnome.org/). Code of conduct incidents can be reported to the Code of Conduct Committee.