Hosting Requirements
====================

The following rules apply to all software projects that are hosted on gnome.org infrastructure:

#. The project must be free/open source software.
#. It must use GTK/GNOME technologies.
#. It must be maintained, and already have had at least one public release.
#. To the best of your knowledge, it must not infringe on patents (most gnome.org servers are in the US).
