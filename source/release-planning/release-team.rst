Release Team
============

`The GNOME release team <https://gitlab.gnome.org/Teams/Releng/releng-docs>`_ is a committee of the GNOME Foundation Board of Directors. The primary responsibility of the release team is managing GNOME releases, including:

* producing and maintaining `GNOME OS <https://os.gnome.org/>`_
* producing and maintaining the `GNOME Flatpak runtimes <https://docs.flatpak.org/en/latest/available-runtimes.html#gnome>`_
* managing GNOME's 6-month release schedule
* running the `app organization project <https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/README.md>`_
* running `GNOME Security <https://security.gnome.org/>`_

The release team is responsible for the release of GNOME as a whole (`example <https://discourse.gnome.org/t/gnome-45-2-released/18358/1>`_), as opposed to releasing individual projects like GNOME Shell or GNOME Clocks, but may also occasionally release individual projects if needed when maintainers are not active.

Membership
----------

`Release team membership <https://gitlab.gnome.org/Teams/Releng/releng-docs/-/blob/main/Membership.md>`_ is normally by invitation and recommendation of existing members when one person leaves. The GNOME Foundation Board of Directors does have the power to select its members, but historically it has not exercised this power. If you wish to join, contact any current member.

Not more than 40% of release team members can directly or indirectly have the same affiliate, similar to section 2.d of the GNOME Foundation bylaws.

Contact
-------

* `Discourse <https://discourse.gnome.org/tag/release-team>`_
* `Matrix channel <https://matrix.to/#/#release-team:gnome.org>`_
