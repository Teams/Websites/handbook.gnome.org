Change Submission
=================

This page provides a high-level overview of how to submit code changes to projects that are hosted on `gitlab.gnome.org <https://gitlab.gnome.org>`_.

Prerequisites
-------------

To propose changes to a module hosted on GNOME's GitLab instance, you will need:

* A system where you can develop and make your changes
* To be logged into gitlab.gnome.org
* An SSH key that you have uploaded to your GitLab account (see :doc:`SSH Keys </infrastructure/ssh-keys>`)
* The ability to build and test the software module that you are going to make changes to (see :doc:`building </development/building>`)
* A basic understanding of how to use `Git version control <https://docs.gitlab.com/ee/topics/git/>`_

Process steps
-------------

The following are the basic steps required to submit code or other changes:

#. In `gitlab.gnome.org <https://gitlab.gnome.org>`_, find the project that you want to change
#. If there isn't an issue report for the thing that you want to improve, :doc:`create one </issues/reporting>`
#. Create your own personal copy of the project by forking it (see `GitLab's forking workflow guide <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>`_)
#. Create a local clone of your fork
#. Make your changes in the local clone:

   * Create and switch to a new branch
   * Make your changes
   * Build and test your changes
   * Commit your changes with an appropriate :doc:`commit message </development/commit-messages>`
   * Push your branch to your fork in GitLab

#. In GitLab, `create a merge request (MR) <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merge-changes-back-upstream>`_ to have the branch in your fork merged into the upstream main branch. (Remember to reference the issue that the MR will fix.)
#. Go through the MR review process:

   * Wait for a maintainer to review your MR
   * When the MR is reviewed, you will likely need to change it in response to the reviewers comments
   * Once all MR comments are resolved, your changes will be merged
