Board of Directors
==================

The Board of Directors governs the GNOME Foundation, the non-profit organization behind the GNOME Project.

Current directors
-----------------

* Erik Albers
* Philip Chimento
* Pablo Correa Gomez
* Allan Day
* Federico Mena Quintero
* Robert McQueen
* Karen Sandler
* Julian Sparber

Director responsibilities
-------------------------

* Attend board meetings (currently a two-hour meeting every every month) - see `board attendance policy <https://gitlab.gnome.org/Teams/Board/-/wikis/Policies/Attendance>`_.
* Attend the two days of in-person meetings prior to GUADEC.
* Promptly respond to board-list, particularly votes that are held there.
* Occasionally take on board tasks. These are typically small and often require you to liaise with other individuals or organizations. However, sometimes they are larger in scope.
* Don't block initiatives: don't volunteer to do tasks that you're incapable of doing. If you don't think you have time to complete a task, let the rest of the board know.
* Represent the board when interacting with community members. If someone raises concerns or issues in your presence, respond appropriately. Consider whether it's an issue that needs to be brought to the rest of the board. Remember that you're an elected representative.
* Keep confidential discussions private. This includes legal discussions or conversations with the Advisory Board.

The board also includes a number of :doc:`positions with their own responsibilities </foundation/board-of-directors/officers>`.

Meetings and minutes
--------------------

The board has its regular meeting on the second Monday of every month.

The Board publishes its meeting minutes in for archival at https://wiki.gnome.org/FoundationBoard/Minutes.

The secretary of the board (understood to include either the secretary, vice-secretary, or their deputy) is responsible for recording the minutes during board meetings.

During each meeting, it is customary for the Board to review and finalize the minutes of its last meeting, and to publish them as soon as possible.  Given the frequency of Foundation Board meetings (once every month), Foundation members should expect to see published minutes approximately one month after the meeting to which they correspond.

Confidential topics
-------------------

In many nonprofit organizations, it is common practice for the minutes of the board meetings to be confidential. Given the mission of the GNOME Foundation, the board should aspire to more openness than this common practice, and historically the GNOME Foundation has published its board meeting minutes publicly by default, except for topics where the board saw a need for confidentiality.

Sometimes a topic discussed in the board meeting should not be included in the published minutes. This is at the discretion of the board, and can be for reasons of individual or organizational privacy, or it can be due to a need to speak candidly or critically of a person, situation, or organization. Example confidential topics include, but are not limited to, the following:

* Discussions of personal conflicts between Foundation members.
* Discussions of the personal finances of Foundation members, such as individual travel sponsorship applications.
* Discussions about the hiring, compensation, or job performance of Foundation staff.
* Discussions of relationships between the GNOME Foundation and other organizations.
* Discussions about recruiting individual Foundation members to volunteer positions.
* Discussions of ongoing legal proceedings involving the GNOME Foundation or its members or staff.

Finally, another guiding principle is that when someone speaks with the expectation that they are speaking in confidence, a record of the conversation must not be published retroactively in the future.

If applicable, published meeting minutes may contain a summary of confidential topics, that omits any confidential details.

There are no “temporarily confidential” topics. It happens often that a topic contains no confidential information, but should not be published until some action item is completed: e.g. an announcement is made, or correspondence is answered. In that case, if the action is not completed when it is time to publish the minutes, the foundation-announce list should be notified.

Who to contact when
-------------------

The GNOME Foundation has staff and committees that handle much of the GNOME Foundation's business. Most inquiries about GNOME Foundation business should be directed to info@gnome.org.

For questions about licensing the GNOME Foundation's trademarks, please contact licensing@gnome.org.

The board of directors can be contacted at board-list@gnome.org. They should be contacted for inquiries about committees, policies, and board meetings. The board is also the last point of escalation in case you have an issue that you feel has not been satisfactorily handled elsewhere in the GNOME Foundation.

For more tips, see `ways to help the board help you <https://blogs.gnome.org/zana/2015/11/21/ways-to-help-the-board-help-you/>`_.

.. toctree::
   :hidden:

   board-of-directors/officers
   board-of-directors/previous-boards
