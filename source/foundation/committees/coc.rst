Code of Conduct Committee
=========================

The Code of Conduct Committee has been appointed by the GNOME Foundation Board of Directors, with the following responsibilities:

* Ensuring that GNOME events follow the GNOME Foundation's events codes of conduct.
* Respond to incident reports, in accordance with the incident response guidelines.
* Keeping records of code of conduct issues, and tracking repeat incidents.

The committee has the power to take action against individuals at GNOME events, including but not restricted to:

* Issuing warnings
* Banning individuals from events
* Halting or cancelling talks
* Removing individual privileges and responsibilities

Permanent sanctions against individuals, including the removal of Foundation membership, must be approved by the Board of Directors. The committee is authorized to speak on behalf of the GNOME Foundation on code of conduct issues.

Additional responsibilities include:

* Developing and maintaining GNOME's incident response guidelines.
* Seeking ways to improve GNOME's events code of conduct, and proposing policy changes to the Board of Directors for approval.
* Developing a code of conduct for online behavior, to be proposed to the Board of Directors.
* Encouraging and facilitating positive conduct through programs and activities.
* Promoting awareness of GNOME's codes of conduct and ensuring that community members have the tools and training to follow them.
* Assisting with public relations surrounding code of conduct issues.

Contact
-------

You can report violations to GNOME's code of conduct to this email address:

conduct@gnome.org

The people in the "Membership" section below will receive what you write to that address.

Membership
----------

Committee membership is by invitation only. Members are appointed and removed by the GNOME Foundation Board of Directors. The current appointed members are: 

* Anisa Kuci
* Federico Mena Quintero
* Michael Downey
* Rosanna Yuen

Resources
---------

.. This list ought to include a link to the photography policy and transparency reports. However, at the time of writing, it is unclear where these will 

* `Code of Conduct <https://conduct.gnome.org/>`_
* `Transparency reports <https://conduct.gnome.org/transparency/>`_, released every three months
