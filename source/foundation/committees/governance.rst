Governance Committee
====================

Current members:

* Erik Albers
* Allan Day
* Federico Mena Quintero
* Karen Sandler 

Committee Charter
=================

The following charter was approved by the Board of Directors on 10 February 2021.

The Governance Committee is a standing committee of the GNOME Foundation Board of Directors. It assists the GNOME Foundation Board of Directors to fulfil its responsibilities, specifically in relation to governance of the GNOME Foundation and recruitment of board candidates.

Key responsibilities
~~~~~~~~~~~~~~~~~~~~

The responsibilities of the Governance Committee include:

* Developing and maintaining the GNOME Foundation's conflict of interest policy
* Monitoring and reviewing overall board performance, particularly with regards to critical board responsibilities; where necessary it should take or recommend steps to resolve performance issues
* Leading and facilitating periodic board assessments
* Creating and implementing the board development plan
* Identifying and recruiting potential directors
* Developing and maintaining the onboarding process for new directors
* Ensuring that directors have knowledge and understanding of board fiduciary responsibilities and tax-emeption laws
* Ensuring that board governance documentation is up to date, including documentation on elections, committees, board roles and responsibilities 

Committee composition
~~~~~~~~~~~~~~~~~~~~~

The Governance Committee is composed of at least four and no more than six members. Members must be Directors of the Board, and should include either the board president or chair.

Committee members are to be appointed by the Board of Directors as set out in the Foundation bylaws.

The chair of the governance committee is to be appointed by a majority decision of the committee members.

Committee meetings
~~~~~~~~~~~~~~~~~~

The Governance Committee determines the frequency with which it meets. This should be no less than quarterly. As of September 2021, meetings occur monthly, to be held shortly after the full Board of Directors meeting.

Standing agenda items, in addition to any current topics, are:

* Evaluation of previous board meeting
* Board training opportunities
* Future candidate development
* Review of ongoing tasks
* Set date and time of next meeting 

Authorization and limitations of power
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Governance Committee has no power or authority to act on behalf of the full Board of Directors.
