New Issue Template
==================

It is recommended that modules use a default issue template. To do this, copy the text below into a file at the following location in your project: ``./gitlab/issue_templates/Default.md``

The template should be adapted to suit your project.

::

  <--!
  Welcome to the <module name> tracker. Please carefully read the issue reporting guidelines before completing your issue report: https://handbook.gnome.org/issues/reporting.html

  * Make sure to search to see if there is an existing report for your issue
  * Do not use this issue tracker to request new features or functionality - instead please create a Discourse post using the <module> tag
  * Always makes sure to describe your personal experience of the issue, and to include specifics of what happened
  * Make sure to include any necessary supplementary information, such as a stack trace for crashes or screenshots of interface issues

  Please complete the affected version information below, before describing your issue.
  -->
  
  # Affected version
  
  * Distribution, including version (example: "Ubuntu 20.04"):
  * <insert module name> version (example: "45.1"):
  * Package format and distributor (example: "Flatpak from Flathub"):
  
  # Issue description
