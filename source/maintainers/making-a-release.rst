Making a Release
================

Releases are how maintainers tell the world—or, more accurately, downstream developers—that it's time to update their dependencies. These guidelines explain how maintainers can generate these releases.


Setting up GitLab CI/CD pipeline and a signing key
--------------------------------------------------

If your project does not already have a release CI pipeline, you will need to :doc:`create one <release-pipeline>`.

If you do not already have a GPG key set up for signing releases, you will need to :doc:`set one up <signing-releases>`.

Before tagging the release
--------------------------

The following steps are typically done on the branch that will be released. However, if your project is configured to disallow pushing to this branch, it will need to happen on a separate branch which is later merged.

* Ensure that the project is building and passing tests.

* Update the version number if necessary (in ``meson.build`` or equivalent).

* For a library, update the library versioning if it is not automatically computed from the project version. Library versioning is passed to the ``soversion`` and ``version`` parameters of Meson's `library method <https://mesonbuild.com/Reference-manual_functions.html#library>`__. The library versioning represent's the library's API level, which is often separate from the project version.

* Update the ``README`` if necessary.

* If the module is an application, add a ``<release>`` entry in the AppStream metadata file (refer to the `AppStream documentation <https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html>`__).

  * Optionally, read through the Git commit log and come up with a bullet point for each significant change and credit the person(s) who did the work. Remember that the AppStream metadata is presented by GUI tools to the user. Some tips:

    * Try to make the bullet points short and snappy but also understandable for a general user (not always easy).
    * If several entries relate to the same change, only use a single bullet point.

* The AppStream specification does not deal with alphabetical components inside a version, so whenever releasing a development snapshot (``alpha``, ``beta``, or ``rc``) you will need to use tilde as a separator for the ``version`` attribute in the ``release`` element, e.g. ``43.alpha`` becomes ``43~alpha``; ``44.beta`` becomes ``44~beta``; and ``45.rc`` becomes ``45~rc``.

  * You also have the option to not list development snapshots in the AppStream metadata, and instead make a full release description for the first stable release.

* Update the ``NEWS`` file:
  
  * The ``NEWS`` file is generally used by packagers and integrators, so you can be more verbose than in the AppStream metadata.
  * Optionally, you can use `this script <https://gitlab.gnome.org/pwithnall/gitlab-changelog/>`__ for generating a ``NEWS`` file from your Git history.
  * You might wish to mention updated dependencies, since it makes life easier for packagers.
  * You might use a format similar to the following:

:: 

  Version 43.0
  ------------
  * Dependency update
  * Some change
  * A bug fix
  * Small maintenance tasks
  * Translation updates

* Commit these changes using ``git commit --gpg-sign``.

  * Signing the commit for each version allows that release to be verified. See :doc:`signing releases </maintainers/signing-releases>` for information on how to set up commit signing.

* Push your changes. Or, if you if you are working on a development branch, merge it into the branch which will be released.

Tagging as the signal for the release
-------------------------------------

The release automation is triggered by pushing an annotated tag to the GitLab repository. The tag should be named according to the version number of the release.

The following is typically done on the branch that will be released. Create an annotated tag:

#. The best way to do this is ``git evtag sign <tag>``, using `git-evtag <https://github.com/cgwalters/git-evtag>`_. This provides strong signing guarantees.
#. If you can’t do that, do it the basic way: ``git tag -s <tag>``.
#. Or, worse: ``git tag -a <tag>``.

This will launch an editor for you to enter the message to use for the tag. Write the name and version of the project:

::

  GNOME Foo 43.0

Once the tag is created, push it to the GitLab repository with ``git push origin <tag>``. The pipeline will be triggered and the release process will start.

Monitoring the release
----------------------

When the tag is created in the GitLab repository, the release process will automatically start. The jobs will generate the tarball and upload it to the GNOME server. You can monitor the release from the GitLab interface.

Once the job has finished, verify that the release was successful by checking that it is listed in `download.gnome.org/sources <https://download.gnome.org/sources/>`_ and in the module's GitLab project under releases.
